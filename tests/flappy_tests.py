import pytest
from shapes import Ball
from shapes import Bird
from games_interface import ScreenSettings


def test_value_error_ball():
    with pytest.raises(ValueError):
        Ball(-5, "blue", 0, 5)


def test_value_error_bird():
    with pytest.raises(ValueError):
        Bird(-5, "blue", 0, 5)


def test_bird_move_right():
    test_bird = Bird(5, "black", 5, 5)
    test_bird.vector.move(10, 0)
    assert test_bird.vector.x == 15 and test_bird.vector.y == 5


def test_bird_move_left():
    test_bird = Bird(5, "black", 5, 5)
    test_bird.vector.move(-10, 0)
    assert test_bird.vector.x == -5 and test_bird.vector.y == 5


def test_bird_move_up():
    test_bird = Bird(5, "black", 5, 5)
    test_bird.vector.move(0, 10)
    assert test_bird.vector.x == 5 and test_bird.vector.y == 15


def test_bird_in_frame_false():
    test_bird = Bird(5, "black", 5, 5)
    test_bird.vector.move(-500, 10)
    screen_settings = ScreenSettings(left_border=-200, right_border=200, down_border=-200, up_border=200)
    assert screen_settings.is_target_inside(test_bird.vector) is False


def test_bird_in_frame_true():
    test_bird = Bird(5, "black", 5, 5)
    test_bird.vector.move(100, 10)
    screen_settings = ScreenSettings(left_border=-200, right_border=200, down_border=-200, up_border=200)
    assert screen_settings.is_target_inside(test_bird.vector) is True
