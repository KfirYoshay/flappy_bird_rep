import math


class Vector:
    """This class will represent a vector with x,y coordinates"""

    def __init__(self, x: int, y: int):
        self._x = x
        self._y = y

    def __sub__(self, other):
        return Vector(self.x - other.x, self.y - other.y)

    def __abs__(self):
        return abs(math.sqrt(math.pow(self.x, 2) + math.pow(self.y, 2)))

    @property
    def x(self) -> int:
        return self._x

    @x.setter
    def x(self, value: int):
        self._x = value

    @property
    def y(self) -> int:
        return self._y

    @y.setter
    def y(self, value: int):
        self._y = value

    def move(self, x_to_move: int, y_to_move: int):
        self.x += x_to_move
        self.y += y_to_move

    def copy(self):
        """Return copy of vector"""
        type_self = type(self)
        return type_self(self.x, self.y)

    def __repr__(self):
        return "{},{}".format(self.x,self.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y
