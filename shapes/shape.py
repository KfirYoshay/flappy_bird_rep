from shapes import Vector
from abc import ABC


class Shape(ABC):
    """This class will represent a shape in the game (bird or ball)"""

    def __init__(self, size: int, color: str, x: int, y: int):
        if size <= 0:
            raise ValueError
        self._vector = Vector(x, y)
        self._size = size
        self._color = color

    @property
    def vector(self) -> Vector:
        return self._vector

    @property
    def size(self) -> int:
        return self._size

    @property
    def color(self) -> str:
        return self._color

    @color.setter
    def color(self, value: str):
        self._color = value
