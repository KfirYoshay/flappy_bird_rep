from random import randint
from shapes import Shape


class Ball(Shape):
    """This class will represent a Ball in the game"""
    """"""
    def __init__(self, size: int, color: str, x: int, y: int):
        super().__init__(size, color, x, y)
        self._speed = randint(1, 10)

    @property
    def speed(self) -> int:
        return self._speed

