from shapes import Shape


class Bird(Shape):
    """This class will represent a bird in the game"""

    def __init__(self, size: int, color: str, x: int, y: int):
        super().__init__(size, color, x, y)
