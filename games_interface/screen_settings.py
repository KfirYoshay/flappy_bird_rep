from shapes import Vector


class ScreenSettings:
    def __init__(self, left_border: int, right_border: int, down_border: int, up_border: int):
        self._left_border = left_border
        self._right_border = right_border
        self._up_border = up_border
        self._down_border = down_border

    def __repr__(self):
        return "({},{}),({},{})".format(self.left_border, self.right_border, self.down_border, self.up_border)

    @property
    def left_border(self) -> int:
        return self._left_border

    @property
    def right_border(self) -> int:
        return self._right_border

    @property
    def up_border(self) -> int:
        return self._up_border

    @property
    def down_border(self) -> int:
        return self._down_border

    def is_target_inside(self, target_to_check: Vector, include_border=False):
        "Return True if target_to_check within screen."
        if include_border is True:
            return self.left_border <= target_to_check.x <= self.right_border and self.down_border <= target_to_check.y \
                   <= self.up_border
        return self.left_border < target_to_check.x < self.right_border and self.down_border < target_to_check.y \
               < self.up_border
