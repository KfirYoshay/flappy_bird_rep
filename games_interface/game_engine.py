from games import FlappyBirdGame, SnakeGame, CannonGame
from games_interface import ScreenSettings, logger
import turtle


class GameEngine:
    def __init__(self):
        logger.name = __name__
        logger.debug("GameEngine Constructor called")
        self._game_option = None

    @property
    def game_option(self) -> str:
        return self._game_option

    @game_option.setter
    def game_option(self, value: str):
        self._game_option = value

    def validate_game_option(self):
        """This function will validate the user game input"""
        if not self.game_option.isdigit() or int(self.game_option) < 1 or int(self.game_option) > 3:
            logger.error("Game choice must be 1-3")
            return False
        return True

    def get_game_option(self):
        """This function will receive from user a game input"""
        screen = turtle.Screen()
        while self.game_option is None or self.validate_game_option() is False:
            self.game_option = screen.textinput("Welcome to games!",
                                                "Enter a game number 1-3:\n1) Flappy\n2) Snake\n3) Cannon")
            logger.info("User choice of game {}".format(self.game_option))

    def run_game(self):
        """This function will run the game"""
        if self.game_option is None:
            self.get_game_option()

        if self.game_option == "1":
            logger.info("Running Flappy game")
            screen_settings = ScreenSettings(left_border=-200, right_border=200, down_border=-200, up_border=200)
            flappy_game = FlappyBirdGame(bird_size=10, bird_color="blue", balls_color="black", balls_probability=10,
                                         screen_settings=screen_settings)
            flappy_game.start_game()

        if self.game_option == "2":
            logger.info("Running Snake game")
            screen_settings = ScreenSettings(left_border=-200, right_border=190, down_border=-200, up_border=190)
            snake_game = SnakeGame(snake_size=9, snake_color='blue', screen_settings=screen_settings)
            snake_game.start_game()

        if self.game_option == "3":
            logger.info("Running Cannon game")
            screen_settings = ScreenSettings(left_border=-200, right_border=200, down_border=-200, up_border=200)
            cannon_game = CannonGame(canon_ball_size=6, canon_ball_color='red', canon_ball_speed=300,
                                     canon_ball_gravity=25, screen_settings=screen_settings)
            cannon_game.start_game()
