from random import randrange, randint
from games_interface import ScreenSettings, logger
from shapes import Vector, Ball
from turtle import up, goto, down, color, update, clear, setup, hideturtle, \
    ontimer, tracer, done, write, dot, onscreenclick


logger.name = __name__


class CannonGame:
    """This class will initialize the game parameters"""

    def __init__(self, canon_ball_size: int, canon_ball_color: str, canon_ball_speed: int, canon_ball_gravity: int,
                 screen_settings: ScreenSettings):
        self._ball = Ball(canon_ball_size, canon_ball_color, screen_settings.left_border, screen_settings.down_border)
        self._speed = Vector(0, 0)
        self._targets = []
        self._score = 0
        self._cannon_ball_speed = canon_ball_speed
        self._cannon_ball_gravity = canon_ball_gravity
        self._screen_settings = screen_settings
        logger.name = __name__
        logger.debug("CannonGame Constructor called")

    @property
    def canon_ball_speed(self) -> int:
        return self._cannon_ball_speed

    @property
    def screen_settings(self) -> ScreenSettings:
        return self._screen_settings

    @property
    def canon_ball_gravity(self) -> int:
        return self._cannon_ball_gravity

    @property
    def score(self) -> int:
        return self._score

    @score.setter
    def score(self, value: int):
        self._score = value

    @property
    def ball(self) -> Ball:
        return self._ball

    @ball.setter
    def ball(self, value: Ball):
        self._ball = value

    @property
    def speed(self) -> Vector:
        return self._speed

    @speed.setter
    def speed(self, value: Vector):
        self._speed = value

    @property
    def targets(self) -> list:
        return self._targets

    def print_score_to_screen(self):
        up()
        goto(self.screen_settings.left_border, self.screen_settings.down_border)
        down()
        color("blue")
        write("Score : {}".format(self.score), align="left", move=False, font=("Ariel", 13, "normal"))

    def fire_cannon(self, x, y):
        "Respond to screen tap."
        if not self.screen_settings.is_target_inside(self.ball.vector):
            logger.info("tap")
            self.ball.vector.x = self.screen_settings.left_border + 1
            self.ball.vector.y = self.screen_settings.down_border + 1
            self.speed.x = (x + self.canon_ball_speed) / self.canon_ball_gravity
            self.speed.y = (y + self.canon_ball_speed) / self.canon_ball_gravity

    def draw(self):
        "Draw ball and targets."
        clear()
        for target in self.targets:
            up()
            goto(target.x, target.y)
            down()
            dot(20, 'blue')
        if self.screen_settings.is_target_inside(self.ball.vector):
            up()
            goto(self.ball.vector.x, self.ball.vector.y)
            down()
            dot(self.ball.size, self.ball.color)
        update()

    def move_targets(self):
        if randrange(40) == 0:
            y = randrange(self.screen_settings.left_border + 50, self.screen_settings.up_border - 50)
            target = Vector(self.screen_settings.right_border, y)
            self.targets.append(target)

        for target in self.targets:
            target.x -= randint(1, 5)

    def move_ball(self):
        if self.screen_settings.is_target_inside(self.ball.vector):
            self.speed.y -= 0.35
            self.ball.vector.move(self.speed.x, self.speed.y)

    def check_if_ball_didnt_hit_target(self):
        dupe = self.targets.copy()
        self.targets.clear()

        for target in dupe:
            if abs(target - self.ball.vector) > 13:
                self.targets.append(target)
            else:
                self.score += 1
                logger.info("Hit a ball! score = {}".format(self.score))

    def check_for_targets_on_screen(self):
        for target in self.targets:
            if not self.screen_settings.is_target_inside(target):
                return

    def game_progress(self):
        self.move_targets()
        self.move_ball()
        self.check_if_ball_didnt_hit_target()
        self.draw()
        self.check_for_targets_on_screen()
        update()
        self.print_score_to_screen()
        ontimer(self.game_progress, 50)

    def start_game(self):
        logger.info("Canon game started")
        setup(420, 420, 370, 0)
        hideturtle()
        up()
        tracer(False)
        onscreenclick(self.fire_cannon)
        self.game_progress()
        done()
        logger.info("Canon game ended")
