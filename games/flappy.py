from random import randint, randrange
from time import sleep
from games_interface import ScreenSettings, logger
from shapes import Bird, Ball
from turtle import setup, hideturtle, up, tracer, onscreenclick, listen, onkey, ontimer, clear, goto, dot, update, done \
    , write, color


class FlappyBirdGame:
    """This class will initialize the game parameters"""
    """bird_size - size of bird in pixels"""
    """bird_color - color of the bird"""
    """balls_color - color of all the balls"""
    """balls_probability - new ball creation probability, for example 10 means there is 1 to 10 chance for new ball"""

    def __init__(self, bird_size: int, bird_color: str, balls_color: str, balls_probability: int,
                 screen_settings: ScreenSettings):
        self._bird = Bird(bird_size, bird_color, 0, 0)
        self._balls = []
        self._score = 0
        self._balls_size = randint(20, 40)
        self._balls_color = balls_color
        self._balls_probability = balls_probability
        self._screen_settings = screen_settings
        logger.name = __name__
        logger.debug("FlappyBirdGame Constructor called")

    @property
    def balls_size(self) -> int:
        return self._balls_size

    @property
    def screen_settings(self) -> ScreenSettings:
        return self._screen_settings

    @property
    def balls_probability(self) -> int:
        return self._balls_probability

    @property
    def balls_color(self) -> str:
        return self._balls_color

    @property
    def score(self) -> int:
        return self._score

    @score.setter
    def score(self, value: int):
        self._score = value

    @property
    def bird(self) -> Bird:
        return self._bird

    @property
    def balls(self) -> list:
        return self._balls

    def increase_score(self):
        self.score += 1

    def tap(self, x, y):
        """Move bird up in response to screen tap."""
        self.bird.vector.move(0, 30)
        logger.info("tap")

    def right(self):
        """Move bird right in response to right arrow key."""
        self.bird.vector.move(30, 0)
        logger.info("Moved right")

    def left(self):
        """Move bird left in response to left arrow key."""
        self.bird.vector.move(-30, 0)
        logger.info("Moved left")

    def up(self):
        """Move bird up in response to up arrow key."""
        self.bird.vector.move(0, 30)
        logger.info("Moved up")

    def down(self):
        """Move bird down in response to down arrow key."""
        self.bird.vector.move(0, -30)
        logger.info("Moved down")

    def draw(self, alive: bool):
        """Draw screen objects."""
        clear()
        goto(self.bird.vector.x, self.bird.vector.y)

        if not alive:
            self.bird.color = 'red'
        dot(self.bird.size, self.bird.color)

        for ball in self.balls:
            goto(ball.vector.x, ball.vector.y)
            dot(ball.size, ball.color)
        update()

    def drop_bird_position(self):
        self.bird.vector.y -= 5

    def move_balls_position(self):
        for ball in self.balls:
            ball.vector.x -= ball.speed

    def try_to_create_new_ball(self):
        if randrange(self.balls_probability) == 0:
            y = randrange(self.screen_settings.left_border + 1, self.screen_settings.up_border - 1)
            ball = Ball(self.balls_size, self._balls_color, self.screen_settings.right_border - 1, y)
            self.balls.append(ball)

    def remove_balls_that_reached_border(self):
        while len(self.balls) > 0 and not self.screen_settings.is_target_inside(self.balls[0].vector):
            self.balls.pop(0)

    def check_for_collision(self):
        for ball in self.balls:
            if abs(ball.vector - self.bird.vector) < 15:
                self.draw(False)
                logger.info("Game over! Hit a ball")
                logger.info("Total score = {}".format(self.score))
                logger.info("Flappy game ended")
                self.show_score()
                sleep(3)
                exit(0)

    def check_if_bird_in_frame(self):
        if not self.screen_settings.is_target_inside(self.bird.vector):
            self.draw(False)
            logger.info("Game over! out of frame")
            logger.info("Total score = {}".format(self.score))
            logger.info("Flappy game ended")
            self.show_score()
            sleep(3)
            exit(0)

    def check_if_game_over(self):
        self.check_if_bird_in_frame()
        self.check_for_collision()

    def show_score(self):
        goto(self.screen_settings.left_border, self.screen_settings.down_border)
        color("blue")
        write("Score : {}".format(self.score), align="left", move=False, font=("Ariel", 13, "normal"))

    def game_progress(self):
        """Update object positions."""

        self.drop_bird_position()
        self.move_balls_position()
        self.try_to_create_new_ball()
        self.remove_balls_that_reached_border()
        self.check_if_game_over()
        self.increase_score()
        self.draw(True)
        self.show_score()
        ontimer(self.game_progress, 50)

    def start_game(self):
        logger.info("Flappy game started")
        setup(420, 420, 370, 0)
        hideturtle()
        up()
        tracer(False)
        onscreenclick(self.tap)
        listen()
        onkey(self.left, "Left")
        onkey(self.right, "Right")
        onkey(self.up, "Up")
        onkey(self.down, "Down")
        self.game_progress()
        done()
