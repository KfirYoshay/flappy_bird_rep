from random import randrange
from shapes import Vector
from games_interface import ScreenSettings,logger
from turtle import up, goto, down, color, begin_fill, forward, left, end_fill, update, clear, setup, hideturtle, \
    ontimer, tracer, listen, onkey, done, write
from time import sleep


class SnakeGame:
    """This class will initialize the game parameters"""
    """snake_size - size of snake in pixels"""
    """snake_color - color of the snake"""

    def __init__(self, snake_size: int, snake_color: str, screen_settings: ScreenSettings):
        self._food = Vector(0, 0)
        self._snake_body = [Vector(10, 0)]
        self._aim = Vector(0, -10)
        self._snake_size = snake_size
        self._snake_color = snake_color
        self._screen_settings = screen_settings
        self._next_head = None
        logger.name = __name__
        logger.debug("SnakeGame Constructor called")

    @property
    def next_head(self) -> Vector:
        return self._next_head

    @property
    def screen_settings(self) -> ScreenSettings:
        return self._screen_settings

    @next_head.setter
    def next_head(self, value: Vector):
        self._next_head = value

    @property
    def food(self) -> Vector:
        return self._food

    @property
    def snake_body(self) -> list:
        return self._snake_body

    @property
    def aim(self) -> Vector:
        return self._aim

    @property
    def snake_size(self) -> int:
        return self._snake_size

    @property
    def snake_color(self) -> str:
        return self._snake_color

    def print_score_to_screen(self):
        up()
        goto(self.screen_settings.left_border, self.screen_settings.down_border)
        down()
        color("blue")
        write("Score : {}".format(len(self.snake_body)), align="left", move=False, font=("Ariel", 13, "normal"))

    def change_snake_direction(self, x, y):
        "Change snake direction."
        self.aim.x = x
        self.aim.y = y

    def draw_square(self, vector_position: Vector, color_to_draw: str):
        up()
        goto(vector_position.x, vector_position.y)
        down()
        color(color_to_draw)
        begin_fill()
        for count in range(4):
            forward(self.snake_size)
            left(90)
        end_fill()

    def set_next_head(self):
        self.next_head = self._snake_body[-1].copy()
        self.next_head.move(self.aim.x, self.aim.y)

    def check_if_head_outside_frame(self):
        return not self.screen_settings.is_target_inside(self.next_head,include_border=True)

    def check_if_snake_eat_itself(self):
        return self.next_head in self.snake_body

    def check_if_game_over(self):
        if self.check_if_head_outside_frame() or self.check_if_snake_eat_itself():
            logger.info("Game over!: Snake size={}".format(len(self.snake_body)))
            self.draw_square(self.next_head, 'red')
            update()
            sleep(3)
            exit(0)

    def change_food_position(self):
        self.food.x = randrange(-15, 15) * 10
        self.food.y = randrange(-15, 15) * 10

    def check_if_snake_eat_food(self):
        if self.next_head == self.food:
            logger.info("Snake:{}".format(len(self.snake_body)))
            self.change_food_position()
        else:
            self.snake_body.pop(0)
        clear()

    def redraw_snake(self):
        for body in self.snake_body:
            self.draw_square(body, self.snake_color)

    def redraw_food(self):
        self.draw_square(self.food, 'green')

    def game_progress(self):
        "Move snake forward one segment."
        self.set_next_head()
        self.check_if_game_over()
        self.snake_body.append(self.next_head)
        self.check_if_snake_eat_food()
        self.redraw_snake()
        self.redraw_food()
        update()
        self.print_score_to_screen()
        ontimer(self.game_progress, 100)

    def start_game(self):
        logger.info("Snake game started")
        setup(420, 420, 370, 0)
        hideturtle()
        tracer(False)
        listen()
        onkey(lambda: self.change_snake_direction(10, 0), 'Right')
        onkey(lambda: self.change_snake_direction(-10, 0), 'Left')
        onkey(lambda: self.change_snake_direction(0, 10), 'Up')
        onkey(lambda: self.change_snake_direction(0, -10), 'Down')
        self.game_progress()
        done()
        logger.info("Snake game ended")
