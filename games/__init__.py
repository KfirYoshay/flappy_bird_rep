from .flappy import FlappyBirdGame
from .snake import SnakeGame
from .cannon import CannonGame
