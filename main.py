from games_interface import GameEngine, logger


def main():
    logger.name = __name__
    logger.info("-" * 50)
    game = GameEngine()
    game.run_game()


if __name__ == '__main__':
    main()
